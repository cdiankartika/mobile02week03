import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
// ini js
import quotes from '../../data/quotes';
// ini buat interface terima datanya
import{ Quote } from'../../data/quote.interface';
import{ QuotesPage } from '../quotes/quotes';

@IonicPage()
@Component({
  selector: 'page-library',
  templateUrl: 'library.html',
})
export class LibraryPage implements OnInit{
  // variabel untuk nampung js
  quoteCollection:{ category:string, quotes: Quote[], icon:string} [];
  // ini buat nge phus
  quotesPage = QuotesPage;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ngOnInit(){
    this.quoteCollection = quotes;
    //console.log(this.quoteCollection);
  }
}
