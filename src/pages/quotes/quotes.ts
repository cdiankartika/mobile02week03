import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams ,AlertController} from 'ionic-angular';
import{ Quote } from'../../data/quote.interface';
import { QuotesService } from '../../service/service';




@IonicPage()
@Component({
  selector: 'page-quotes',
  templateUrl: 'quotes.html',
})
export class QuotesPage implements OnInit{
  // terima data
  quotes:any;
  constructor(public navCtrl: NavController, private navParams:NavParams, private quoteservice: QuotesService, private alertCtrl: AlertController){

  }

  ngOnInit(){
    this.quotes = this.navParams.data;
    console.log(this.quotes);

  }
  
  addToFavorite(quote){
    this.quoteservice.addQuoteToFavorites(quote);
  }
  onShowAlert() {
    const alert = this.alertCtrl.create({
      title: 'Add Quote',
      message:'Are you sure you want to add teh quote for favorites?',
      buttons:[
        {
          text:'OK',
          handler:()=>{
            console.log("Yes is clicked.");
            console.log(this.quoteservice);
          }
        },
        {
          text:'NO',
          role:'cancel',
          handler:()=>{
            console.log("No is clicked.")
          }
        }
      ]
    });
    alert.present();
  }
}
